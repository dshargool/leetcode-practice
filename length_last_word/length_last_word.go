package main

import (
	"fmt"
	"strings"
)

func main() {
	s := "Hello World"
	words := strings.Fields(s)

	fmt.Println(len(words[len(words)-1]))
}
