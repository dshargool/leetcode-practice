def isPalindrome(x: int) -> bool:
    num_str = str(x)
    rev_str = [None] * len(num_str)
    for i in range(len(num_str)):
        rev_str[i] = num_str[len(num_str) - 1 - i]
        print(rev_str)
    print(str(rev_str))
    return ''.join(rev_str) == num_str

print(isPalindrome(121))

