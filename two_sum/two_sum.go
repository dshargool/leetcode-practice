package main

import "fmt"

func main() {
	nums := []int{2, 7, 11, 15}
	target := 9

	for i := 0; i < len(nums); i++ {
		for j := 0; j < len(nums); j++ {
			if nums[i]+nums[j] == target {
				fmt.Println("Target at: ", i, j)
			}
		}
	}
	nums = []int{3, 2, 4}
	target = 6

	for i := 0; i < len(nums); i++ {
		for j := 0; j < len(nums); j++ {
			if nums[i]+nums[j] == target && i != j {
				fmt.Println("Target at: ", i, j)
			}
		}
	}
}
