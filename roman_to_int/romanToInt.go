package main

import (
	"errors"
	"fmt"
	"os"
)

func roman_to_int(roman byte) (int, error) {
	switch roman {
	case 'I':
		return 1, nil
	case 'V':
		return 5, nil
	case 'X':
		return 10, nil
	case 'L':
		return 50, nil
	case 'C':
		return 100, nil
	case 'D':
		return 500, nil
	case 'M':
		return 1000, nil
	}
	return 0, errors.New("invalid roman numeral")
}

func convert_slice(vals string) []int {
	var converted []int

	for i := range vals {
		val, err := roman_to_int(vals[i])
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		} else {
			converted = append(converted, val)
		}
	}

	return converted
}

func sum_slice(slice []int) int {
	var sum int
	for i := range slice {
		if i > 0 {
			if slice[i] > slice[i-1] {
				j := i
				for j >= 2 && slice[j-1] == slice[j-2] {
					sum = sum - 2*slice[i-1]
					j--
				}
				sum = sum - 2*slice[i-1]
			}
		}
		sum += slice[i]
	}
	return sum
}

func main() {
	s1 := "III"
	fmt.Println(sum_slice(convert_slice(s1)))
	s2 := "LVIII"
	fmt.Println(sum_slice(convert_slice(s2)))
	s3 := "MCMXCIV"
	fmt.Println(sum_slice(convert_slice(s3)))
}
